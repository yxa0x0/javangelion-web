// @ts-ignore

/* eslint-disable */
import { request } from 'umi';
/** 获取当前的用户 GET /api/currentUser */


export async function currentUser(options) {
  return request('/api/currentUser', {
    method: 'GET',
    ...(options || {}),
  });
}
/** 退出登录接口 POST /api/login/outLogin */

export async function outLogin(options) {
  return request('/api/login/outLogin', {
    method: 'POST',
    ...(options || {}),
  });
}
/** 登录接口 POST /api/login/account */

// export async function login(body, options) {
//   await digestPassword(body.password)
//   return request(baseURL + '/user/login', {
//     method: 'POST',
//     headers: {
//       'Content-Type': 'application/json',
//       'Accept': 'application/json'
//     },
// //    data: body,
//     params: {
//       stu_id: body.username,
//       pwd: await digestPassword(body.password)
//     },
//     ...(options || {}),
//   });
// }

export async function login(body, options) {
  return request('/api/login/account', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 此处后端没有提供注释 GET /api/notices */

export async function getNotices(options) {
  return request('/api/notices', {
    method: 'GET',
    ...(options || {}),
  });
}
/** 获取规则列表 GET /api/user */

export async function user(params, options) {
  return request('/api/user', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}
/** 新建规则 PUT /api/user */

export async function updateuser(data, options) {
  return request('/api/user', {
    headers: {
      'Content-Type': 'application/json',
      // 'Accept': 'application/json'
    },
    method: 'PATCH',
    data,
    ...(options || {}),
  });
}
/** 新建规则 POST /api/user */

export async function adduser(data, options) {
  console.log(options)
  let resp
  for (let col of data) {
    resp = await request('/api/user', {
      headers: {
        'Content-Type': 'application/json',
        // 'Accept': 'application/json'
      },
      method: 'POST',
      data: col,
      ...(options || {}),
    });
  }
  return resp
}
/** 删除规则 DELETE /api/user */

export async function removeuser(ids, options) {
  let ret
  for (let id of ids) {
    console.log('delete ' + id)
    ret = await request('/api/user', {
      method: 'DELETE',
      params: {'id': id},
      ...(options || {}),
    });
  }
  return ret
  // return request('/api/user', {
  //   method: 'DELETE',
  //   ...(options || {}),
  // });
}


/** 获取规则列表 GET /api/course */

export async function course(params, options) {
  return request('/api/course', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}
/** 新建规则 PUT /api/course */

export async function updatecourse(data, options) {
  return request('/api/course', {
    headers: {
      'Content-Type': 'application/json',
      // 'Accept': 'application/json'
    },
    method: 'PATCH',
    data,
    ...(options || {}),
  });
}
/** 新建规则 POST /api/course */

export async function addcourse(data, options) {
  console.log(options)
  let resp
  for (let col of data) {
    resp = await request('/api/course', {
      headers: {
        'Content-Type': 'application/json',
        // 'Accept': 'application/json'
      },
      method: 'POST',
      data: col,
      ...(options || {}),
    });
  }
  return resp
}
/** 删除规则 DELETE /api/course */

export async function removecourse(ids, options) {
  let ret
  for (let id of ids) {
    console.log('delete ' + id)
    ret = await request('/api/course', {
      method: 'DELETE',
      params: {'id': id},
      ...(options || {}),
    });
  }
  return ret
  // return request('/api/course', {
  //   method: 'DELETE',
  //   ...(options || {}),
  // });
}

/** 获取规则列表 GET /api/activity */

export async function activity(params, options) {
  return request('/api/activity', {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}
/** 新建规则 PUT /api/activity */

export async function updateactivity(data, options) {
  return request('/api/activity', {
    headers: {
      'Content-Type': 'application/json',
      // 'Accept': 'application/json'
    },
    method: 'PATCH',
    data,
    ...(options || {}),
  });
}
/** 新建规则 POST /api/activity */

export async function addactivity(data, options) {
  console.log(options)
  let resp
  for (let col of data) {
    resp = await request('/api/activity', {
      headers: {
        'Content-Type': 'application/json',
        // 'Accept': 'application/json'
      },
      method: 'POST',
      data: col,
      ...(options || {}),
    });
  }
  return resp
}
/** 删除规则 DELETE /api/activity */

export async function removeactivity(ids, options) {
  let ret
  for (let id of ids) {
    console.log('delete ' + id)
    ret = await request('/api/activity', {
      method: 'DELETE',
      params: {'id': id},
      ...(options || {}),
    });
  }
  return ret
  // return request('/api/activity', {
  //   method: 'DELETE',
  //   ...(options || {}),
  // });
}

export function activityCourse(params, options) {}
export function setToken() {}
export function setUserID() {}
export function setChoose() {}
export function userCourse() {}
export function alterPwd() {}
export function stuListInCourse() {}
