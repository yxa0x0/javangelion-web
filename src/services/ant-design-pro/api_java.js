import { setTokenSourceMapRange } from 'typescript';
import { request } from 'umi';

const baseURL = 'https://w.api.yxa0.xyz:25595/api'

export default {
  token: '',
  userID: 0,
  async user(params, options) {
    console.log(params, options)
    return request(baseURL + `/user/list/${params.current || 1}/${params.pageSize || 20}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      },
      data: params,
      ...(options || {}),
    });
  },

  async adduser(data, options) {
    return request(baseURL + '/user', {
      headers: {
        'Content-Type': 'application/json',
        // 'Accept': 'application/json'
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
      },
      method: 'POST',
      data,
      ...(options || {}),
    });
  },

  async updateuser(user, options) {
    return request(baseURL + `/user/${user.id}`, {
      headers: {
        'Content-Type': 'application/json',
        // 'Accept': 'application/json'
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
      },
      method: 'PATCH',
      data: {
        user,
      },
      ...(options || {}),
    });
  },

  async removeuser(ids, options) {
    return request(baseURL + `/user/delete`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        // 'Accept': 'application/json'
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
      },
      data: ids,
      ...(options || {}),
    })
  },

  async addcourse(data, options) {
    return request(baseURL + '/course', {
      headers: {
        'Content-Type': 'application/json',
        // 'Accept': 'application/json'
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
      },
      method: 'POST',
      data,
      ...(options || {}),
    });
  },

  async updatecourse(course, options) {
    return request(baseURL + `/course/${course.id}`, {
      headers: {
        'Content-Type': 'application/json',
        // 'Accept': 'application/json'
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
      },
      method: 'PATCH',
      data: course,
      ...(options || {}),
    });
  },

  async removecourse(ids, options) {
    return request(baseURL + `/course/delete`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        // 'Accept': 'application/json'
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
      },
      data: ids,
      ...(options || {}),
    })
  },

  async course(params, options) {
    console.log(params, options)
    return request(baseURL + `/course/list/${params.current || 1}/${params.pageSize || 20}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
      },
      data: params,
      ...(options || {}),
    });
  },

  async addactivity(data, options) {
    console.log(data)
    return request(baseURL + '/activity', {
      headers: {
        'Content-Type': 'application/json',
        // 'Accept': 'application/json'
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
      },
      method: 'POST',
      data,
      ...(options || {}),
    });
  },

  async updateactivity(activity, options) {
    return request(baseURL + `/activity/${activity.id}`, {
      headers: {
        'Content-Type': 'application/json',
        // 'Accept': 'application/json'
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
      },
      method: 'PATCH',
      data: activity,
      ...(options || {}),
    });
  },

  async removeactivity(ids, options) {
    return request(baseURL + `/activity/delete`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        // 'Accept': 'application/json'
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
      },
      data: ids,
      ...(options || {}),
    })
  },

  async activity(params, options) {
    console.log(params, options)
    return request(baseURL + `/activity/list/${params.current || 1}/${params.pageSize || 20}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
      },
      data: params,
      ...(options || {}),
    });
  },

  async activityCourse(params, options) {
    return request(baseURL + `/activity/${params.aid}/class/${params.current || 1}/${params.pageSize || 20}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
      },
      data: params,
      ...(options || {}),
    });
  },

  async login(body, options) {
    return request(baseURL + '/user/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      data: {
        username: body.username,
        password: body.password,
      },
      ...(options || {}),
    });
  },

  async currentUser(options) {
    let data = await request(baseURL + `/user/${localStorage.getItem('uid')}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
      },
      ...(options || {}),
    });
    // teio! teio! teio!
    return {success: true, data: {...data, avatar: "https://s4.ax1x.com/2021/12/31/T46F6x.png"}}
  },

  setToken(token) {
    localStorage.setItem('token', token)
    console.log(token)
  },
  setUserID(uid) {
    console.log(uid)
    localStorage.setItem('uid', uid)
  },
  async setChoose(aid, cid, op, options) {
    return request(baseURL + `/choose/${aid}/${cid}/0/${op}`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
      },
      ...(options || {}),
    });
  },

  async userCourse(params, options) {
    return request(baseURL + `/user/0/course/0/0`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
      },
      ...(options || {}),
    });
  },

  async alterPwd(pwd, new_pwd, options) {
    return request(baseURL + `/user/0`, {
      headers: {
        'Content-Type': 'application/json',
        // 'Accept': 'application/json'
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
      },
      method: 'PATCH',
      data: {
        pwd,
        new_pwd
      },
      ...(options || {}),
    });
  },

  async stuListInCourse(activityId, courseId, params, options) {
    return request(baseURL + `/choose/activity/${activityId}/course/${courseId}/${params.current || 1}/${params.pageSize || 20}`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
      },
      ...(options || {}),
    });
  }
}
