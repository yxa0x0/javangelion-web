import * as apiMock from './api_mock'
import apiJava from './api_java'

const useJava = true;

let wrapper = {};
(() => {
    for (let fnName in apiMock) {
        if (useJava && fnName in apiJava) {
            wrapper[fnName] = apiJava[fnName]
        } else {
            wrapper[fnName] = apiMock[fnName]
        }
    }
})()

export default wrapper;