import { useIntl, FormattedMessage } from 'umi';


export default {
    0: {
      text: <FormattedMessage id="pages.course.0" defaultMessage="0" />,
      status: '0',
    },
    1: {
      text: <FormattedMessage id="pages.course.1" defaultMessage="1" />,
      status: '1',
    },
    2: {
      text: <FormattedMessage id="pages.course.2" defaultMessage="2" />,
      status: '2',
    },
    3: {
      text: <FormattedMessage id="pages.course.3" defaultMessage="3" />,
      status: '3',
    },
    4: {
      text: <FormattedMessage id="pages.course.4" defaultMessage="4" />,
      status: '4',
    },
    5: {
      text: <FormattedMessage id="pages.course.5" defaultMessage="5" />,
      status: '5',
    },
    6: {
      text: <FormattedMessage id="pages.course.6" defaultMessage="6" />,
      status: '6',
    },
    7: {
      text: <FormattedMessage id="pages.course.7" defaultMessage="7" />,
      status: '7',
    },
    8: {
      text: <FormattedMessage id="pages.course.8" defaultMessage="8" />,
      status: '8',
    },
    9: {
      text: <FormattedMessage id="pages.course.9" defaultMessage="9" />,
      status: '9',
    },
    10: {
      text: <FormattedMessage id="pages.course.10" defaultMessage="10" />,
      status: '10',
    },
    11: {
      text: <FormattedMessage id="pages.course.11" defaultMessage="11" />,
      status: '11',
    },
    12: {
      text: <FormattedMessage id="pages.course.12" defaultMessage="12" />,
      status: '12',
    },
    13: {
      text: <FormattedMessage id="pages.course.13" defaultMessage="13" />,
      status: '13',
    },
    14: {
      text: <FormattedMessage id="pages.course.14" defaultMessage="14" />,
      status: '14',
    },
    15: {
      text: <FormattedMessage id="pages.course.15" defaultMessage="15" />,
      status: '15',
    },
    16: {
      text: <FormattedMessage id="pages.course.16" defaultMessage="16" />,
      status: '16',
    },
    17: {
      text: <FormattedMessage id="pages.course.17" defaultMessage="17" />,
      status: '17',
    },
    18: {
      text: <FormattedMessage id="pages.course.18" defaultMessage="18" />,
      status: '18',
    },
    19: {
      text: <FormattedMessage id="pages.course.19" defaultMessage="19" />,
      status: '19',
    },
    20: {
      text: <FormattedMessage id="pages.course.20" defaultMessage="20" />,
      status: '20',
    },
    21: {
      text: <FormattedMessage id="pages.course.21" defaultMessage="21" />,
      status: '21',
    },
    22: {
      text: <FormattedMessage id="pages.course.22" defaultMessage="22" />,
      status: '22',
    },
    23: {
      text: <FormattedMessage id="pages.course.23" defaultMessage="23" />,
      status: '23',
    },
    24: {
      text: <FormattedMessage id="pages.course.24" defaultMessage="24" />,
      status: '24',
    },
    25: {
      text: <FormattedMessage id="pages.course.25" defaultMessage="25" />,
      status: '25',
    },
    26: {
      text: <FormattedMessage id="pages.course.26" defaultMessage="26" />,
      status: '26',
    },
    27: {
      text: <FormattedMessage id="pages.course.27" defaultMessage="27" />,
      status: '27',
    },
    28: {
      text: <FormattedMessage id="pages.course.28" defaultMessage="28" />,
      status: '28',
    },
    29: {
      text: <FormattedMessage id="pages.course.29" defaultMessage="29" />,
      status: '29',
    },
    30: {
      text: <FormattedMessage id="pages.course.30" defaultMessage="30" />,
      status: '30',
    },
    31: {
      text: <FormattedMessage id="pages.course.31" defaultMessage="31" />,
      status: '31',
    },
    32: {
      text: <FormattedMessage id="pages.course.32" defaultMessage="32" />,
      status: '32',
    },
    33: {
      text: <FormattedMessage id="pages.course.33" defaultMessage="33" />,
      status: '33',
    },
    34: {
      text: <FormattedMessage id="pages.course.34" defaultMessage="34" />,
      status: '34',
    },
    35: {
      text: <FormattedMessage id="pages.course.35" defaultMessage="35" />,
      status: '35',
    },
    36: {
      text: <FormattedMessage id="pages.course.36" defaultMessage="36" />,
      status: '36',
    },
    37: {
      text: <FormattedMessage id="pages.course.37" defaultMessage="37" />,
      status: '37',
    },
    38: {
      text: <FormattedMessage id="pages.course.38" defaultMessage="38" />,
      status: '38',
    },
    39: {
      text: <FormattedMessage id="pages.course.39" defaultMessage="39" />,
      status: '39',
    },
    40: {
      text: <FormattedMessage id="pages.course.40" defaultMessage="40" />,
      status: '40',
    },
    41: {
      text: <FormattedMessage id="pages.course.41" defaultMessage="41" />,
      status: '41',
    },
}