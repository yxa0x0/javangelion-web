import { Modal, Button, message } from 'antd';
import ReactDOM from 'react-dom';
import ProTable from '@ant-design/pro-table';
import { useIntl, FormattedMessage } from 'umi';
import api from '@/services/ant-design-pro/api';
import { PlusOutlined } from '@ant-design/icons';
import React, { useState, useRef } from 'react';
import { PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import ProForm, { ModalForm, DrawerForm, ProFormText, ProFormSelect, ProFormTextArea } from '@ant-design/pro-form';
import ProDescriptions from '@ant-design/pro-descriptions';
import { reject, toInteger } from 'lodash';
import { disposeEmitNodes } from 'typescript';
import CourseTime from '@/pages/CourseTime';
import userListInCourse from './UserListInCourse';
import UserListInCourse from './UserListInCourse';


const handleChoose = async function (activityId, courseId, status) {
  if (status) {
    status = 'set'
  } else {
    status = 'unset'
  }
  const hide = message.loading('正在请求')
  await api.setChoose(activityId, courseId, status, {
    errorHandler: function (error) {
      hide()
      if (error.response.status == 410) {
        message.info('信息过期')
      } else {
        message.error('请求fail')
        console.log(error)
      }
    }
  })
  hide()
  message.success('请求成功')
}

const ChoosePage = (props) => {

  const intl = useIntl();
  const actionRef = useRef();
  const [userListVisible, setUserListVisible] = useState(false);
  const [currentCourseID, setCurrentCourseID] = useState(0);

  const columns = [
    {
      title:
        <FormattedMessage id="pages.course.name" defaultMessage="name" />,
      dataIndex: 'name',
      hideInSearch: true,
      render: (dom, entity) => {
        return (
          <a
            onClick={() => {
              setCurrentCourseID(entity.id)
              setUserListVisible(true)
              console.log(userListVisible)
            }}
          >
            {dom}
          </a>
        );
      },
    },
    {
      title:
        <FormattedMessage id="pages.course.id" defaultMessage="courseId" />,
      dataIndex: 'id',
      valueType: 'digit',
    },
    {
      title:
        <FormattedMessage id="pages.course.showAvailable" defaultMessage="showAvailable" />,
      dataIndex: 'show_available',
      valueType: 'switch',
      hideInTable: true,
      initialValue: true,
    },
    {
      title:
        <FormattedMessage id="pages.course.teacher" defaultMessage="teacher" />,
      dataIndex: 'teacher',
      hideInSearch: true,
      //valueType: 'textarea',
    },
    {
      title:
        <FormattedMessage id="pages.course.grade" defaultMessage="grade" />,
      dataIndex: 'grade',
      hideInSearch: true,
      //valueType: 'textarea',
    },
    {
      title:
        <FormattedMessage id="pages.course.current" defaultMessage="current" />,
      dataIndex: 'select_cnt',
      hideInSearch: true,
      //valueType: 'textarea',
    },
    {
      title:
        <FormattedMessage id="pages.course.total" defaultMessage="total" />,
      dataIndex: 'total',
      hideInSearch: true,
      //valueType: 'textarea',
    },
    {
      title:
        <FormattedMessage id="pages.course.selected" defaultMessage="selected" />,
      dataIndex: 'selected',
      hideInSearch: true,
      valueEnum: {
        0: {
          text: <FormattedMessage id="pages.course.notSelect" defaultMessage="notSelect" />,
          status: 'notSelect',
        },
        1: {
          text: <FormattedMessage id="pages.course.selected" defaultMessage="selected" />,
          status: 'selected',
        },
      },
      //search: false,
      //valueType: 'textarea',
    },
    {
      title:
        <FormattedMessage id="pages.course.time" defaultMessage="time" />,
      dataIndex: 'time',
      hideInSearch: true,
      //hideInForm: true,
      valueEnum: CourseTime,
      castType: "int", // for format convert when load from csv
    },
    {
      title: <FormattedMessage id="pages.searchTable.titleOption" defaultMessage="Operating" />,
      dataIndex: 'option',
      valueType: 'option',
      render: (_, record) => ((record.select_cnt == record.total && record.selected == 0) ? [
        <FormattedMessage id="pages.item.unavailable" defaultMessage="unavailable" />
      ] : ([
        <a
          key="edit"
          onClick={async () => {
            setCurrentRow(record);
            await handleChoose(props.ActivityID, record.id, !record.selected)
            actionRef.current?.reloadAndRest?.();
          }}
        >
          {record.selected == 0 ? <FormattedMessage id="pages.item.choose" defaultMessage="choose" />
            : <FormattedMessage id="pages.item.unchoose" defaultMessage="unchoose" />}
        </a>,
      ])),
      ignore: (true)
    },
  ];
  const [currentRow, setCurrentRow] = useState();
  const [selectedRowsState, setSelectedRows] = useState([]);
  const [showDetail, setShowDetail] = useState(false);


  return (
    <>
      <Modal
        width={1000}
        visible={props.updateChoosePageVisible}
        footer={null}
        destroyOnClose={true}
        onCancel={props.onCancel}>
        {/* {props.ActivityID} */}
        <ProTable
          headerTitle={intl.formatMessage({
            id: 'menu.list.course-list',
            defaultMessage: 'Enquiry form',
          })}
          actionRef={actionRef}
          rowKey="id"
          search={{
            //labelWidth: 120,
          }}
          request={(params, options) => {
            params.show_available = params.show_available ? 1 : 0;
            params.aid = props.ActivityID;
            return api.activityCourse(params, options);
          }}
          columns={columns}
          pagination={{ pageSizeOptions: [10, 20, 50, 100, 500, 1000] }}
        />
        <UserListInCourse visible={userListVisible} onCancel={()=>setUserListVisible(false)} ActivityID={props.ActivityID} courseID={currentCourseID}/>
      </Modal>
    </>
  )
}

export default ChoosePage;