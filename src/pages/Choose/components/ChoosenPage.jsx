import { Modal, Button, message } from 'antd';
import ReactDOM from 'react-dom';
import ProTable from '@ant-design/pro-table';
import { useIntl, FormattedMessage } from 'umi';
import api from '@/services/ant-design-pro/api';
import { PlusOutlined } from '@ant-design/icons';
import React, { useState, useRef } from 'react';
import { PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import ProForm, { ModalForm, DrawerForm, ProFormText, ProFormSelect, ProFormTextArea } from '@ant-design/pro-form';
import ProDescriptions from '@ant-design/pro-descriptions';
import { reject, toInteger } from 'lodash';
import { disposeEmitNodes } from 'typescript';
import CourseTime from '@/pages/CourseTime';


const handleChoose = async function(activityId, courseId, status) {
  if (status) {
    status = 'set'
  } else {
    status = 'unset'
  }
  const hide = message.loading('正在请求')
  await api.setChoose(activityId, courseId, status, {
    errorHandler: function(error) {
      hide()
      if (error.response.status == 410) {
        message.info('信息过期')
      } else {
        message.error('请求fail')
        console.log(error)
      }
    }
  })
  hide()
  message.success('请求成功')
}

const ChoosenPage = (props) => {

  const intl = useIntl();
  const actionRef = useRef();
  const columns = [
    {
      title:
      <FormattedMessage id="pages.course.name" defaultMessage="name" />,
      dataIndex: 'name',
    },
    {
      title:
        <FormattedMessage id="pages.course.teacher" defaultMessage="teacher" />,
      dataIndex: 'teacher',
      //valueType: 'textarea',
    },
    {
      title:
        <FormattedMessage id="pages.course.grade" defaultMessage="grade" />,
      dataIndex: 'grade',
      //valueType: 'textarea',
    },
    {
      title:
        <FormattedMessage id="pages.course.current" defaultMessage="current" />,
      dataIndex: 'select_cnt',
      //valueType: 'textarea',
    },
    {
      title:
        <FormattedMessage id="pages.course.total" defaultMessage="total" />,
      dataIndex: 'total',
      //valueType: 'textarea',
    },
    {
      title:
        <FormattedMessage id="pages.course.time" defaultMessage="time" />,
      dataIndex: 'time',
      //hideInForm: true,
      valueEnum: CourseTime,
      castType: "int", // for format convert when load from csv
    },
    {
      title: <FormattedMessage id="pages.searchTable.titleOption" defaultMessage="Operating" />,
      dataIndex: 'option',
      valueType: 'option',
      render: (_, record) => ((record.select_cnt == record.total && record.selected == 0) ? [
        <FormattedMessage id="pages.item.unavailable" defaultMessage="unavailable" />
      ] : ([
        <a
          key="edit"
          onClick={async () => {
            setCurrentRow(record);
            await handleChoose(props.ActivityID, record.id, !record.selected)
            actionRef.current?.reloadAndRest?.();
          }}
        >
          {record.selected == 0 ? <FormattedMessage id="pages.item.choose" defaultMessage="choose" />
            : <FormattedMessage id="pages.item.unchoose" defaultMessage="unchoose" />}
        </a>,
      ])),
      ignore: (true)
    },
  ];
  const [currentRow, setCurrentRow] = useState();

  return (
    <Modal
      width={1000}
      visible={props.updateChoosenPageVisible}
      footer={null}
      destroyOnClose={true}
      onCancel={props.onCancel}>
      <ProTable
        headerTitle={intl.formatMessage({
          id: 'menu.list.course-list',
          defaultMessage: 'Enquiry form',
        })}
        actionRef={actionRef}
        rowKey="id"
        search={false}
        request={(params, options) => {params.aid = props.ActivityID; params.selected = 1; return api.activityCourse(params, options);}}
        columns={columns}
        pagination={{ pageSizeOptions: [10, 20, 50, 100, 500, 1000] }}
      />
    </Modal>
  )
}

export default ChoosenPage;