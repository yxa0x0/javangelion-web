import { PlusOutlined, ArrowUpOutlined, ArrowDownOutlined } from '@ant-design/icons';
import { Button, message, Upload, Input, Drawer, Modal } from 'antd';
import React, { useState, useRef } from 'react';
import { useIntl, FormattedMessage } from 'umi';
import { PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import ProTable from '@ant-design/pro-table';
import ProForm, { ModalForm, DrawerForm, ProFormText, ProFormSelect, ProFormTextArea } from '@ant-design/pro-form';
import ProDescriptions from '@ant-design/pro-descriptions';
import api from '@/services/ant-design-pro/api';
import { reject, toInteger } from 'lodash';
import { generateForm, castType } from '@/pages/generateForm'
import csvSaver from '@/components/csvSaver';

const UserListInCourse = (props) => {
  /**
   * @en-US Pop-up window of new window
   * @zh-CN 新建窗口的弹窗
   *  */
  /**
   * @en-US The pop-up window of the distribution update window
   * @zh-CN 分布更新窗口的弹窗
   * */

  const actionRef = useRef();
  const [currentRow, setCurrentRow] = useState();
  const [selectedRowsState, setSelectedRows] = useState([]);
  /**
   * @en-US International configuration
   * @zh-CN 国际化配置
   * */

  const intl = useIntl();
  const refreshTable = () => actionRef.current?.reloadAndRest?.();
  const columns = [
    {
      title:
        (
          <FormattedMessage
            id="pages.user.stuID"
          />
        ),
      dataIndex: 'stu_id',
      tip: 'The rule name is the unique key',
    },
    {
      title:
        <FormattedMessage id="pages.user.name" defaultMessage="name" />,
      dataIndex: 'name',
      //valueType: 'textarea',
    },
    {
      title:
        <FormattedMessage id="pages.user.grade" defaultMessage="grade" />,
      dataIndex: 'grade',
      //valueType: 'textarea',
    },
    {
      title:
        <FormattedMessage id="pages.user.gender" defaultMessage="gender" />,
      dataIndex: 'gender',
      //hideInForm: true,
      valueEnum: {
        0: {
          text: <FormattedMessage id="pages.user.female" defaultMessage="female" />,
          status: 'Female',
        },
        1: {
          text: <FormattedMessage id="pages.user.male" defaultMessage="male" />,
          status: 'Male',
        },
      },
      castType: "int", // for format convert when load from csv
    },
    {
      title:
        <FormattedMessage id="pages.user.permission" defaultMessage="permission" />,
      dataIndex: 'permission',
      //hideInForm: true,
      valueEnum: {
        1: {
          text: <FormattedMessage id="pages.user.student" defaultMessage="student" />,
          status: 'Student',
        },
        0: {
          text: <FormattedMessage id="pages.user.admin" defaultMessage="admin" />,
          status: 'Admin',
        },
      },
      castType: "int", // for format convert when load from csv
    },
  ];

  const saver = new csvSaver(columns)
  return (
    <Modal
      width={1000}
      visible={props.visible}
      footer={null}
      destroyOnClose={true}
      onCancel={props.onCancel}>
      <ProTable
        headerTitle={intl.formatMessage({
          id: 'menu.list.user-list',
          defaultMessage: 'Enquiry form',
        })}
        actionRef={actionRef}
        rowKey="id"
        search={false}
        request={(params) => api.stuListInCourse(props.ActivityID, props.courseID, params)}
        columns={columns}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
        }}
        pagination={{ pageSizeOptions: [10, 20, 50, 100, 500, 1000] }}
      />
      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.searchTable.chosen" defaultMessage="Chosen" />{' '}
              <a
                style={{
                  fontWeight: 600,
                }}
              >
                {selectedRowsState.length}
              </a>{' '}
              <FormattedMessage id="pages.searchTable.item" defaultMessage="项" />
              &nbsp;&nbsp;
            </div>
          }
        >
          <Button type="primary"
            onClick={() => {
              saver.exportToFile(selectedRowsState)
              setSelectedRows([])
              //actionRef.current?.reloadAndRest?.();
            }}
          >
            <FormattedMessage
              id="pages.items.exportBatch"
              defaultMessage="Batch output to csv"
            />
          </Button>

        </FooterToolbar>
      )}
    </Modal>
  );
};

export default UserListInCourse;
