import { PlusOutlined, ArrowUpOutlined, ArrowDownOutlined } from '@ant-design/icons';
import { Button, message, Upload, Input, Drawer } from 'antd';
import React, { useState, useRef } from 'react';
import { useIntl, FormattedMessage } from 'umi';
import { PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import ProTable from '@ant-design/pro-table';
import ProForm, { ModalForm, DrawerForm, ProFormText, ProFormSelect, ProFormTextArea } from '@ant-design/pro-form';
import ProDescriptions from '@ant-design/pro-descriptions';
import CSV from '../../components/csv';
import api from '@/services/ant-design-pro/api';
import FileSaver from 'file-saver'
import { reject, toInteger } from 'lodash';
import { generateForm, castType } from '../generateForm'
import CoursePage from './components/CoursePage';
import csvSaver from '@/components/csvSaver';

const fromTimeRange = (row) => {
  if ('timeRange' in row) {
    row = { ...row, begin_time: row['timeRange'][0], end_time: row['timeRange'][1] }
    if (!(row.begin_time instanceof Number) && !isNaN(Date.parse(row.begin_time))) {
      row.begin_time = Date.parse(row.begin_time)
    }

    if (!(row.end_time instanceof Number) && !isNaN(Date.parse(row.end_time))) {
      row.end_time = Date.parse(row.end_time)
    }
    delete row['timeRange']
  }

  console.log(row)
  return row
}

const toTimeRange = (rows) => {
  console.log(rows)
  const res = rows.map(row => { row.timeRange = [row.begin_time, row.end_time]; return row; })
  console.log(res)
  return res
}

const getActivity = async (params, options) => {
  let result = await api.activity(fromTimeRange(params), options)
  result.data = toTimeRange(result.data)
  return result
}



/**
 * @en-US Add node
 * @zh-CN 添加节点
 * @param fields
 */


const handleAdd = async (fields) => {
  const hide = message.loading('正在添加');
  console.log(fields)
  try {
    await api.addactivity(fields.map(fromTimeRange));
    hide();
    message.success('Added successfully');
    return true;
  } catch (error) {
    hide();
    message.error('Adding failed, please try again!');
    return false;
  }
};
/**
 * @en-US Update node
 * @zh-CN 更新节点
 *
 * @param fields
 */

const handleUpdate = async (id, fields) => {
  const hide = message.loading('Configuring');

  try {
    await api.updateactivity(fromTimeRange({ 'id': id, ...fields }));
    hide();
    message.success('Configuration is successful');
    return true;
  } catch (error) {
    hide();
    console.log(error)
    message.error('Configuration failed, please try again!');
    return false;
  }
};


/**
 *  Delete node
 * @zh-CN 删除节点
 *
 * @param selectedRows
 */

const handleRemove = async (selectedRows) => {
  const hide = message.loading('正在删除');
  if (!selectedRows) return true;

  try {
    await api.removeactivity(selectedRows.map((row) => row.id));
    hide();
    message.success('Deleted successfully and will refresh soon');
    return true;
  } catch (error) {
    hide();
    message.error('Delete failed, please try again');
    return false;
  }
};

const activityList = () => {
  /**
   * @en-US Pop-up window of new window
   * @zh-CN 新建窗口的弹窗
   *  */
  const [createModalVisible, handleModalVisible] = useState(false);
  /**
   * @en-US The pop-up window of the distribution update window
   * @zh-CN 分布更新窗口的弹窗
   * */
   const [coursePageVisible, handleCoursePageVisible] = useState(false);

  const [updateModalVisible, handleUpdateModalVisible] = useState(false);
  const [showDetail, setShowDetail] = useState(false);
  const actionRef = useRef();
  const [currentRow, setCurrentRow] = useState();
  const [selectedRowsState, setSelectedRows] = useState([]);
  /**
   * @en-US International configuration
   * @zh-CN 国际化配置
   * */

  const intl = useIntl();
  const refreshTable = () => actionRef.current?.reloadAndRest?.();
  const columns = [
    {
      title:
        <FormattedMessage id="pages.activity.name" defaultMessage="name" />,
      dataIndex: 'name',
      render: (dom, entity) => {
        return (
          <a
            onClick={() => {
              console.log(entity)
              setCurrentRow(entity)
              handleCoursePageVisible(true)
            }}
          >
            {dom}
          </a>
        )
      },
    },
    {
      title:
        <FormattedMessage id="pages.activity.active" defaultMessage="active" />,
      dataIndex: 'active',
      ignore: true,
      valueEnum: {
        0: {
          text: <FormattedMessage id="pages.activity.deactivate" defaultMessage="deactivate" />,
          status: '0',
        },
        1: {
          text: <FormattedMessage id="pages.activity.activate" defaultMessage="active" />,
          status: '1',
        },
      },
      castType: "int",
      //valueType: 'textarea',
    },
    {
      title:
        <FormattedMessage id="pages.activity.timeRange" defaultMessage="timeRange" />,
      dataIndex: 'timeRange',
      valueType: 'dateTimeRange',
    },
    {
      title:
        <FormattedMessage id="pages.activity.choose_limit" defaultMessage="choose_limit" />,
      dataIndex: 'choose_limit',
      valueType: 'digit',
      //valueType: 'textarea',
    },
    {
      title:
        <FormattedMessage id="pages.activity.grade" defaultMessage="grade" />,
      dataIndex: 'grade',
      //valueType: 'textarea',
    },
    {
      title: <FormattedMessage id="pages.searchTable.titleOption" defaultMessage="Operating" />,
      dataIndex: 'option',
      valueType: 'option',
      render: (_, record) => [
        <a
          key="edit"
          onClick={() => {
            //handleUpdateModalVisible(true);
            setCurrentRow(record);
            setShowDetail(true)
          }}
        >
          <FormattedMessage id="pages.item.edit" defaultMessage="Edit" />
        </a>,
        <a key="delete"
          onClick={async () => {
            await handleRemove([record]);
            actionRef.current?.reloadAndRest?.();
          }}>
          <FormattedMessage
            id="pages.item.remove"
            defaultMessage="Remove"
          />
        </a>,
      ],
      ignore: (true)
    },
  ];

  const fieldNameToIndex = function (arrOfObject) {
    console.log(arrOfObject)
    return arrOfObject.map(row => Object.keys(row).reduce((obj, key) => {
      const col = columns.find(col => IntlText(col.title) == key)
      if (col) {
        const fieldName = col.dataIndex
        let value = row[key]
        if (col.valueEnum) {
          for (let Enum in col.valueEnum) {
            if (IntlText(col.valueEnum[Enum].text) == value) {
              value = Enum
              break
            }
          }
        }
        obj[fieldName] = value
      }
      return obj
    }, {}))
  }

  const readCSV = async function (file) {
    try {
      let data = await new Promise((resolve, reject) => {
        let reader = new FileReader()
        reader.onload = () => resolve(reader.result)
        reader.reject = reject
        reader.readAsText(file)
      })
      return fieldNameToIndex(toTimeRange(CSV.deserializeRows(data)))
    } catch {
      return;
    }
  }

  const checkColums = function (arrOfObj) {
    for (let col of columns) {
      if (col.ignore === true) {
        continue
      }
      for (let obj of arrOfObj) {
        console.log(col, obj)
        if (!obj.hasOwnProperty(col.dataIndex)) {
          console.log(obj)
          console.log(`don't have ${col.dataIndex}`)
          return false
        }

        //obj[col.dataIndex] = parseInt(obj[col.dataIndex])
        obj[col.dataIndex] = castType(col.castType, obj[col.dataIndex])
      }
    }
    return true
  }

  const uploadProps = {
    accept: ".csv",
    customRequest: async function (option) {
      console.log(option)
      const columns = await readCSV(option.file)
      console.log(columns)
      if (!columns || !checkColums(columns)) {
        console.log('err')
        const err = intl.formatMessage({
          id: 'pages.items.CSVParseError',
          defaultMessage: 'csv parse fail',
        })
        message.error(err)
        option.onError(new Error(err))
        return
      }
      const succ = intl.formatMessage({
        id: 'pages.items.CSVParseSucc',
        defaultMessage: 'csv parse succ',
      })
      if (!await handleAdd(columns)) {
        option.onError(new Error("add fail"))
        return
      }
      option.onSuccess()
      message.success(succ)
      refreshTable()
    }
  }

  const saver = new csvSaver(columns)

  return (
    <PageContainer>
      <ProTable
        headerTitle={intl.formatMessage({
          id: 'menu.list.activity-list',
          defaultMessage: 'Enquiry form',
        })}
        actionRef={actionRef}
        rowKey="id"
        search={{
          labelWidth: 120,
        }}
        toolBarRender={() => [
          // <Button
          //   //type="primary"
          //   key="exportEmpty"
          //   onClick={() => {
          //     saver.exportToFile([columns.reduce((obj, col) => {
          //       obj[col.dataIndex] = null
          //       return obj
          //     }, {})])
          //   }}
          // >
          //   <ArrowDownOutlined /> <FormattedMessage id="pages.items.exportEmpty" defaultMessage="export empty" />
          // </Button>,
          <Button
            //type="primary"
            key="new"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <PlusOutlined /> <FormattedMessage id="pages.searchTable.new" defaultMessage="New" />
          </Button>,
          // <Upload {...uploadProps}>
          //   <Button
          //     type="primary"
          //     key="importBatch"
          //   >
          //     <ArrowUpOutlined /> <FormattedMessage id="pages.items.importBatch" defaultMessage="import batch" />
          //   </Button>,
          // </Upload>
        ]}
        request={getActivity}
        columns={columns}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
        }}
        pagination={{ pageSizeOptions: [10, 20, 50, 100, 500, 1000] }}
      />
      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.searchTable.chosen" defaultMessage="Chosen" />{' '}
              <a
                style={{
                  fontWeight: 600,
                }}
              >
                {selectedRowsState.length}
              </a>{' '}
              <FormattedMessage id="pages.searchTable.item" defaultMessage="项" />
              &nbsp;&nbsp;
              {/* <span>
                <FormattedMessage
                  id="pages.searchTable.totalServiceCalls"
                  defaultMessage="Total number of service calls"
                />{' '}
                {selectedRowsState.reduce((pre, item) => pre + item.callNo, 0)}{' '}
                <FormattedMessage id="pages.searchTable.tenThousand" defaultMessage="万" />
              </span> */}
            </div>
          }
        >
          <Button type="primary"
            onClick={async () => {
              await handleRemove(selectedRowsState);
              setSelectedRows([]);
              actionRef.current?.reloadAndRest?.();
            }}
          >
            <FormattedMessage
              id="pages.searchTable.batchDeletion"
              defaultMessage="Batch deletion"
            />
          </Button>
        </FooterToolbar>
      )}
      <ModalForm
        title=
        {intl.formatMessage({
          id: 'pages.activityTable.Add',
          defaultMessage: 'Add activity',
        })}
        width="800px"
        visible={createModalVisible}
        onVisibleChange={handleModalVisible}
        onFinish={async (value) => {
          const success = await handleAdd([value]);

          if (success) {
            handleModalVisible(false);

            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
      >
        {generateForm(columns, { defaultVal: false })}
      </ModalForm>
      {/* <UpdateForm
        onSubmit={async (value) => {
          const success = await handleUpdate(value);

          if (success) {
            handleUpdateModalVisible(false);
            setCurrentRow(undefined);

            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
        onCancel={() => {
          handleUpdateModalVisible(false);

          if (!showDetail) {
            setCurrentRow(undefined);
          }
        }}
        updateModalVisible={updateModalVisible}
        values={currentRow || {}}
      /> */}

      <DrawerForm
        width={500}
        visible={showDetail}
        onVisibleChange={setShowDetail}
        onFinish={async (data) => {
          console.log(data)
          const id = currentRow.id
          setCurrentRow(undefined);
          setShowDetail(false);
          if (handleUpdate(id, data)) {
            actionRef.current?.reloadAndRest?.();
            return true
          }
          return false
        }}
        title={intl.formatMessage({
          id: 'pages.activityTable.Edit',
          defaultMessage: 'Edit activity',
        })}
        drawerProps={{
          forceRender: true,
          destroyOnClose: true,
        }}
        onClose={() => {
          setCurrentRow(undefined);
          setShowDetail(false);
        }}
      >
        {generateForm(columns, { defaultRow: () => currentRow, disabled: ['stu_id'] })}
      </DrawerForm>

      <CoursePage
        ActivityID={currentRow ? currentRow.id : null}
        coursePageVisible={coursePageVisible}
        onCancel={() => {
          handleCoursePageVisible(false)
          setCurrentRow(null)
        }} />
    </PageContainer>
  );
};

export default activityList;
