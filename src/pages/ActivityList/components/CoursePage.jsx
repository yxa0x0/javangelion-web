import { Modal, Button } from 'antd';
import ProTable from '@ant-design/pro-table';
import { useIntl, FormattedMessage } from 'umi';
import api from '@/services/ant-design-pro/api';
import { ArrowDownOutlined } from '@ant-design/icons';
import { useState, useRef } from 'react';
import { FooterToolbar } from '@ant-design/pro-layout';

import CourseTime from '@/pages/CourseTime';
import csvSaver from '@/components/csvSaver';

const CoursePage = (props) => {

  const intl = useIntl();
  const actionRef = useRef();
  const columns = [
    {
      title:
        <FormattedMessage id="pages.course.name" defaultMessage="name" />,
      dataIndex: 'name',
    },
    {
      title:
        <FormattedMessage id="pages.course.id" defaultMessage="courseId" />,
      dataIndex: 'id',
      //valueType: 'textarea',
    },
    {
      title:
        <FormattedMessage id="pages.course.teacher" defaultMessage="teacher" />,
      dataIndex: 'teacher',
      //valueType: 'textarea',
    },
    {
      title:
        <FormattedMessage id="pages.course.grade" defaultMessage="grade" />,
      dataIndex: 'grade',
      //valueType: 'textarea',
    },
    {
      title:
        <FormattedMessage id="pages.course.current" defaultMessage="current" />,
      dataIndex: 'select_cnt',
      //valueType: 'textarea',
    },
    {
      title:
        <FormattedMessage id="pages.course.total" defaultMessage="total" />,
      dataIndex: 'total',
      //valueType: 'textarea',
    },
    {
      title:
        <FormattedMessage id="pages.course.time" defaultMessage="time" />,
      dataIndex: 'time',
      valueEnum: CourseTime,
      castType: "int", // for format convert when load from csv
    },
  ];
  const [selectedRowsState, setSelectedRows] = useState([]);
  const [res, setRes] = useState({});

  const saver = new csvSaver(columns)

  //消失时清空已选
  const handleCancel = function () {
    props.onCancel();
    setSelectedRows([]);
  }

  return (
    <Modal
      width={1000}
      visible={props.coursePageVisible}
      footer={null}
      destroyOnClose={true}
      onCancel={handleCancel}>
      {selectedRowsState?.length > 0 && (<FooterToolbar
        extra={
          <div>
            <FormattedMessage id="pages.searchTable.chosen" defaultMessage="Chosen" />{' '}
            <a
              style={{
                fontWeight: 600,
              }}
            >
              {selectedRowsState.length}
            </a>{' '}
            <FormattedMessage id="pages.searchTable.item" defaultMessage="项" />
            &nbsp;&nbsp;
          </div>
        }
      >

        <Button type="primary"
          onClick={() => {
            saver.exportToFile(selectedRowsState)
            setSelectedRows([]);
          }}
        >
          <FormattedMessage
            id="pages.items.exportBatch"
            defaultMessage="Batch output to csv"
          />
        </Button>
      </FooterToolbar>)}
      <ProTable
        headerTitle={intl.formatMessage({
          id: 'menu.list.course-list',
          defaultMessage: 'Enquiry form',
        })}
        actionRef={actionRef}
        rowKey="id"
        search={false}
        toolBarRender={() => [
          <Button
            //type="primary"
            key="exportEmpty"
            onClick={() => {
              saver.exportToFile(res)
            }}
          >
            <ArrowDownOutlined /> <FormattedMessage id="pages.items.exportAll" defaultMessage="export all" />
          </Button>,
        ]}
        request={async (params, options) => {
          params.aid = props.ActivityID;
          // params.pageSize=500;
          let msg = await api.activityCourse(params, options);
          setRes(msg.data);
          return msg;
        }}
        columns={columns}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
        }}
        pagination={{ pageSizeOptions: [10, 20, 50, 100, 500, 1000] }}
      />
    </Modal>
  )
}

export default CoursePage;