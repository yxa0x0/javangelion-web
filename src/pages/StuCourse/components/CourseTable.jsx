import { Modal, Button, message } from 'antd';
import ReactDOM from 'react-dom';
import ProTable from '@ant-design/pro-table';
import { useIntl, FormattedMessage } from 'umi';
import api from '@/services/ant-design-pro/api';
import { PlusOutlined } from '@ant-design/icons';
import React, { useState, useRef } from 'react';
import { PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import ProForm, { ModalForm, DrawerForm, ProFormText, ProFormSelect, ProFormTextArea } from '@ant-design/pro-form';
import ProDescriptions from '@ant-design/pro-descriptions';
import { reject, toInteger } from 'lodash';
import { disposeEmitNodes } from 'typescript';
import CourseTime from '@/pages/CourseTime';

export default (props) => {

  const intl = useIntl();
  const actionRef = useRef();
  const columns = [
    {
      title:
      <FormattedMessage id="pages.course.name" defaultMessage="name" />,
      dataIndex: 'name',
    },
    {
      title:
        <FormattedMessage id="pages.course.teacher" defaultMessage="teacher" />,
      dataIndex: 'teacher',
      //valueType: 'textarea',
    },
    {
      title:
        <FormattedMessage id="pages.course.time" defaultMessage="time" />,
      dataIndex: 'time',
      //hideInForm: true,
      valueEnum: CourseTime,
      castType: "int", // for format convert when load from csv
    },
  ];

  console.log(props.courses)

  return (
    <Modal
      width={1000}
      visible={props.visible}
      onCancel={props.onCancel}
      footer={[]}
      destroyOnClose={true}>
      <ProTable
        // headerTitle={intl.formatMessage({
        //   id: 'menu.list.course-list',
        //   defaultMessage: 'Enquiry form',
        // })}
        rowKey="id"
        search={false}
        request={() => ({data: props.courses, total: props.courses.length})}
        columns={columns}
        pagination={false}
        visible={props.visible}
        options={false}
      />
    </Modal>
  )
}
