import ProCard from '@ant-design/pro-card';
import { useIntl, FormattedMessage } from 'umi';
import React, { useState, useRef } from 'react';
import CourseTable from './CourseTable';


export default ({ courses, time }) => {
    const [CourseTableVisible, UpdateCourseTableVisible] = useState(false);
    const intl = useIntl();
    const maxWidth = 200
    
    if (courses.length == 0) {
        return (
            <ProCard title={intl.formatMessage({
                id: 'pages.CourseCard.nocourse',
                defaultMessage: 'nocourse',
            })}
                style={{ maxWidth }} hoverable bordered headerBordered>
                <div>{time}</div>
                <div>*</div>
            </ProCard>
        )
    } else if (courses.length == 1) {
        return (
            <ProCard title={courses[0].name}
                style={{ maxWidth }} hoverable bordered headerBordered>
                <div>{time}</div>
                <div>{courses[0].teacher}</div>
            </ProCard>
        )
    }
    return (
        <>
            <CourseTable courses={courses} visible={CourseTableVisible} onCancel={()=>UpdateCourseTableVisible(false)}></CourseTable>
            <ProCard title={courses.map(c => c.name).join('/')}
                style={{ maxWidth }} hoverable bordered headerBordered
                onClick={() => UpdateCourseTableVisible(true)}>
                <div>{time}</div>
                <div><FormattedMessage id="pages.course.teacher" defaultMessage="teacher" />: {courses.map(c => c.teacher).join('/')}</div>
                <div><FormattedMessage id='pages.CourseCard.detail' defaultMessage="detail"></FormattedMessage></div>
            </ProCard>
        </>
    )

}