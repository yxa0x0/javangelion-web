//有关页面的数据都放在页面的models下，这里举一个获取公司信息的例子
 
//与后台数据的交互需经由service来完成请求任务
//这里有两个服务，一个是请求公司信息的服务，一个是保存修改后公司新信息的服务
//import { queryCompanyInfo,saveCompanyInfo} from '@/services/api';
 

import api from '@/services/ant-design-pro/api';

const StuCourseReq = async function(userId) {
    return api.userCourse()
    // return await new Promise((res) => {
    //     setTimeout(() => {
    //         console.log('fin')
    //         let ret = []
    //         for (let i = 0; i < 42; ++i) {
    //             if (i % 4 == 0) {
    //                 continue
    //             }
    //             ret.push({
    //                 name: '计算机网络',
    //                 teacher:'张天舒',
    //                 time: i
    //             })
    //             if (i % 2 == 0) {
    //                 ret.push({
    //                     name: '计算机网sdafsdfsdfsdfdsf络',
    //                     teacher:'张天舒',
    //                     time: i
    //                 })
    //             }
    //         }
    //         res([{
    //             name: 'first',
    //             id: 233,
    //             course: ret
    //         }, {
    //             name: 'second',
    //             id: 244,
    //             course: []
    //         }])
    //     }, 1000)
    // })
}

export default{
    //这里给models的命名需唯一，否则会报错
    namespace:'stuCourse',
    state:{
        data:[]
    },
    reducers:{
        //要想改变state中的数据，必须通过reducers，
        //payload是参数
        setStuCourse(state,{payload:{data}}){
            // console.log(back);
            return{...state,data};
          },
    },
    effects:{
        *queryStuCourse({payload:{userId}},{put,call}){
            //请求后台服务，介由service中的异步方法
            //const data= yield call(queryCompanyInfo);
            const data = yield call(StuCourseReq, userId);
            // console.log(data);
            //打印一下，看看返回的是什么
           //这里后台返回来一个对象Object {msg: "ok", code: 200, data: Object},
            //这里data.data下放的是我们的企业信息
 
            //将请求到的数据返回给同步的reducers中的backCompanyInfo，
            //后面传参的键一定要和上面的backCompanyInfo接受的名字相同,
 
            //涉及到es6的解析语法，要想解析出相同的值，你的键值必须相对应
 
            yield put({type:"setStuCourse",payload:{data}});  
        },
    },
}