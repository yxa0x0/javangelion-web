import React from 'react';
import ProCard from '@ant-design/pro-card';
import { PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import { connect } from 'dva';
import CourseCard from './components/CourseCard';
import { Col, Row } from 'antd';
import CourseTime from '../CourseTime';
const { Divider } = ProCard;


@connect(({ loading, stuCourse }) => ({
  // 此处用于当 myModel 这个 models 有数据请求行为的时候，loading 为 true，没有请求的时候为 false;
  loadingAll: loading.models.stuCourse,
  // 此处用于 myModel 的 effects 中的 myEffects（即在 model 中发送请求），发生了异步请求行为时为 true，没有请求行为时为 false;
  loadingList: loading.effects['stuCourse/queryStuCourse'],
  // 此处单纯获取 model 中定义的 initState 中的值;
  data: stuCourse.data,
}))

export default class StuCourse extends React.Component {
  constructor(props) {
    super(props);
    this.state = ({
      currentTab: 0,
      dataSet: new Map(),
      loadingList: props.loadingList,
      tablist: []
    })
  }
  courseGroupBy(course, prop) {
    let ret = new Map();
    if (!course) {
      return ret
    }
    for (let c of course) {
      if (!ret.has(c[prop])) {
        ret.set(c[prop], [])
      }
      let res = ret.get(c[prop])
      res.push(c)
      ret.set(c[prop], res)
    }
    return ret
  }

  componentDidMount() {
    //派发异步请求
    //绑定models之后就可解析出dispatch
    const { dispatch } = this.props;

    //向models传参，形式牢记，请求企业信息服务

    dispatch({ type: 'stuCourse/queryStuCourse', payload: { userId: 0 } });
    //这里的dispatch也可以写成Promise形式，后面加.then,可以在dispath结束后再执行相应的操作
    //也很实用，可以进一步探索 

  }
  componentWillReceiveProps(nextProps) {

    //第一次能获取到modals数据的地方,
    //这个生命周期的nextProps能获取到你将要绑定进入props的companyInfo

    console.log(nextProps);
    const tablist = nextProps.data.map(record => ({tab: record.name, key: record.id}))
    console.log(tablist)
    const data = new Map();
    for (let r of nextProps.data) {
      data.set(r.id, r.course)
    }
    //同样，你可以把companyInfo，映射到你这个类的state中去，通过使用this.setState方法
    this.setState({
      dataSet: data,
      loadingList: nextProps.loadingList,
      tablist,
      currentTab: tablist?.[0]?.key || 0,
    })
  }

  render() {
    return (
      <PageContainer loading={this.state.loadingList || false}
        tabList={this.state.tablist}
        onTabChange={key=>{
          this.setState({...this.state, currentTab: parseInt(key)})
        }}>
        {(() => {
          let data = this.courseGroupBy(this.state.dataSet.get(this.state.currentTab) || [], 'time')
          let rows = []
          //let t = 0; t < 6; ++t
          //let day = 0; day < 7; ++day
          for (let t = 0; t < 6; ++t) {
            rows.push((<Row gutter={16}>{(() => {
              let cols = []
              for (let day = 0; day < 7; ++day) {
                let index = day * 6 + t;
                cols.push((
                  <Col span={3}><CourseCard courses={data.get(index) || []} time={CourseTime[index].text}></CourseCard></Col>
                ))
              }
              return cols
            })()}</Row>))
            rows.push((
              <Divider type='horizontal' />
            ))
          }
          return rows
        })()}
        {/* <Row gutter={16}>
          <Col span={3}>
            <CourseCard courses={this.state.data[1] ? [this.state.data[1]] : []} time='下午'></CourseCard>
            <CourseCard courses={this.state.data[1] ? [this.state.data[1]] : []} time='下午'></CourseCard>
          </Col>
          <Col span={3}>
            <CourseCard courses={this.state.data} time='下午'></CourseCard></Col>
        </Row>
        <Divider type='horizontal' />
        <Row gutter={16}>
          <Col span={3}>
            <CourseCard courses={this.state.data} time='下午'></CourseCard>
          </Col>
          <Col span={3}>
            <CourseCard courses={this.state.data[1] ? [this.state.data[1]] : []} time='下午'></CourseCard></Col>
        </Row> */}
      </PageContainer >
    );
  }
}