import { PlusOutlined, HomeOutlined, ContactsOutlined, ClusterOutlined } from '@ant-design/icons';
import { Avatar, Card, Col, Divider, Input, Row, Tag } from 'antd';
import React, { useState, useRef, useLayoutEffect } from 'react';
import { GridContent } from '@ant-design/pro-layout';
import { useIntl, FormattedMessage } from 'umi';
import { useModel } from 'umi';
import styles from './Center.less';
import { PageContainer } from '@ant-design/pro-layout';
import { Menu } from 'antd';
const { Item } = Menu;


const BaseInfo = (props) => {
  const menuMap = {
    base: '基本信息',
    setting: '信息修改',
  };
  const dom = useRef();
  const [initConfig, setInitConfig] = useState({
    mode: 'inline',
    selectKey: 'base',
  });
  const resize = () => {
    requestAnimationFrame(() => {
      if (!dom.current) {
        return;
      }

      let mode = 'inline';
      const { offsetWidth } = dom.current;

      if (dom.current.offsetWidth < 641 && offsetWidth > 400) {
        mode = 'horizontal';
      }

      if (window.innerWidth < 768 && offsetWidth > 400) {
        mode = 'horizontal';
      }

      setInitConfig({ ...initConfig, mode: mode });
    });
  };

  useLayoutEffect(() => {
    if (dom.current) {
      window.addEventListener('resize', resize);
      resize();
    }

    return () => {
      window.removeEventListener('resize', resize);
    };
  }, [dom.current]);



  const intl = useIntl();
  const { initialState } = useModel('@@initialState');
  const { currentUser } = initialState || {};
  const genderValueEnum = {
    0: {
      text: <FormattedMessage id="pages.user.female" defaultMessage="female" />,
      status: 'Female',
    },
    1: {
      text: <FormattedMessage id="pages.user.male" defaultMessage="male" />,
      status: 'Male',
    },
  }
  return (
    <Row gutter={24} justify="center">
      <Col lg={8}>
        <Card
          bordered={false}
          style={{
            marginBottom: 24,
          }}
        >
          <div>
            <div className={styles.avatarHolder}>
              <img alt="" src={currentUser.avatar} />
              <div className={styles.name}>{currentUser.name}</div>
            </div>
            <div className={styles.detail}>
              <p><ContactsOutlined
                style={{
                  marginRight: 8,
                }}
              /><FormattedMessage
                  id="pages.user.stuID"
                />: {currentUser.id}
              </p>
              <p><ContactsOutlined
                style={{
                  marginRight: 8,
                }}
              /><FormattedMessage
                  id="pages.user.grade"
                />: {currentUser.grade}
              </p>
              <p><ContactsOutlined
                style={{
                  marginRight: 8,
                }}
              /><FormattedMessage
                  id="pages.user.gender"
                />: {genderValueEnum[currentUser.gender].text}
              </p>
            </div>
          </div>

        </Card>
      </Col>
    </Row>
  )
}

export default BaseInfo;