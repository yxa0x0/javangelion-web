import { PlusOutlined, HomeOutlined, ContactsOutlined, ClusterOutlined } from '@ant-design/icons';
import { Avatar, Card, Col, Divider, Input, Row, Tag } from 'antd';
import React, { useState, useRef, useLayoutEffect } from 'react';
import { GridContent } from '@ant-design/pro-layout';
import { useIntl, FormattedMessage } from 'umi';
import { useModel } from 'umi';
import styles from './Center.less';
import { PageContainer } from '@ant-design/pro-layout';
import { Menu } from 'antd';
import BaseInfo from './BaseInfo';
import Setting from './Setting';
const { Item } = Menu;


const UserInfo = (props) => {
  const menuMap = {
    base: '基本信息',
    setting: '信息修改',
  };
  const dom = useRef();
  const [initConfig, setInitConfig] = useState({
    selectKey: 'base',
  });


  const renderChildren = () => {
    const { selectKey } = initConfig;

    switch (selectKey) {
      case 'base':
        return <BaseInfo />;

      case 'setting':
        return <Setting />;

      // case 'binding':
      //   return <BindingView />;

      // case 'notification':
      //   return <NotificationView />;

      default:
        return null;
    }
  };

  const intl = useIntl();
  const { initialState } = useModel('@@initialState');
  const { currentUser } = initialState || {};
  const genderValueEnum = {
    0: {
      text: <FormattedMessage id="pages.user.female" defaultMessage="female" />,
      status: 'Female',
    },
    1: {
      text: <FormattedMessage id="pages.user.male" defaultMessage="male" />,
      status: 'Male',
    },
  }
  const tabList = [
    {
      "tab": "基本信息",
      "key": "base"
    },
    {
      "tab": "信息修改",
      "key": "setting"
    },
  ]

  return (
    <PageContainer
      tabList={tabList}
      onTabChange={key => {
        setInitConfig({selectKey: key})
      }}>
        {renderChildren()}
    </PageContainer>
  )
}

export default UserInfo;