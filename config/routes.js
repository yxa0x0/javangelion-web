export default [
  {
    path: '/user',
    layout: false,
    routes: [
      {
        path: '/user',
        routes: [
          {
            name: 'login',
            path: '/user/login',
            component: './user/Login',
          },
        ],
      },
      {
        component: './404',
      },
    ],
  },
  {
    name: 'account.center',
    path: '/account/center',
    component: './user/Info',
    icon: 'smile',
  },
  // {
  //   path: '/welcome',
  //   name: 'welcome',
  //   icon: 'smile',
  //   component: './Welcome',
  // },
  // {
  //   path: '/admin',
  //   name: 'admin',
  //   icon: 'crown',
  //   access: 'canAdmin',
  //   component: './Admin',
  //   routes: [
  //     {
  //       path: '/admin/sub-page',
  //       name: 'sub-page',
  //       icon: 'smile',
  //       component: './Welcome',
  //     },
  //     {
  //       component: './404',
  //     },
  //   ],
  // },
  // {
  //   name: 'list.table-list',
  //   icon: 'table',
  //   path: '/list',
  //   component: './TableList',
  // },
  {
    name: 'list.user-list',
    icon: 'user',
    path: '/userlist',
    component: './UserList',
    access: 'canAdmin',
  },
  {
    name: 'list.course-list',
    icon: 'table',
    path: '/courselist',
    component: './CourseList',
    access: 'canAdmin',
  },
  {
    name: 'list.activity-list',
    icon: 'swap',
    path: '/activitylist',
    component: './ActivityList',
    access: 'canAdmin',
  },
  {
    name: 'list.choose-list',
    icon: 'like',
    path: '/choose',
    component: './Choose',
  },
  {
    name: 'list.stu-course',
    icon: 'appstore',
    path: '/stuCourse',
    component: './StuCourse',
  },
  {
    path: '/',
    redirect: '/account/center',
  },
  {
    component: './404',
  },
];
