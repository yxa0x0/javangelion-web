const Settings = {
  navTheme: 'light',
  // 拂晓蓝
  primaryColor: '#1890ff',
  layout: 'mix',
  contentWidth: 'Fluid',
  fixedHeader: false,
  fixSiderbar: true,
  colorWeak: false,
  title: '教务选课系统',
  pwa: false,
  logo: '/icons/教务中心@2x.svg',
  iconfontUrl: '',
};
export default Settings;
