// eslint-disable-next-line import/no-extraneous-dependencies
import moment from 'moment';
import { parse } from 'url'; // mock tableListDataSource

const genList = (current, pageSize) => {
  const tableListDataSource = [];

  for (let i = 0; i < pageSize; i += 1) {
    const index = (current - 1) * 10 + i;
    tableListDataSource.push({
      gender: i % 2,
      grade: "2018级软件" + i % 10,
      id: i,
      name: "张弟叔" + i,
      permission: 0,
      stu_id: "2018204610" + i,
      // disabled: i % 6 === 0,
      // href: 'https://ant.design',
      // avatar: [
      //   'https://gw.alipayobjects.com/zos/rmsportal/eeHMaZBwmTvLdIwMfBpg.png',
      //   'https://gw.alipayobjects.com/zos/rmsportal/udxAbMEhpwthVVcjLXik.png',
      // ][i % 2],
      // name: `TradeCode ${index}`,
      // owner: '曲丽丽',
      // desc: '这是一段描述',
      // callNo: Math.floor(Math.random() * 1000),
      // status: Math.floor(Math.random() * 10) % 4,
      // updatedAt: moment().format('YYYY-MM-DD'),
      // createdAt: moment().format('YYYY-MM-DD'),
      // progress: Math.ceil(Math.random() * 100),
    });
  }

  tableListDataSource.reverse();
  return tableListDataSource;
};

let tableListDataSource = genList(1, 100);

function getRule(req, res, u) {
  let realUrl = u;

  if (!realUrl || Object.prototype.toString.call(realUrl) !== '[object String]') {
    realUrl = req.url;
  }

  const { current = 1, pageSize = 10 } = req.query;
  const params = parse(realUrl, true).query;
  let dataSource = [...tableListDataSource].slice((current - 1) * pageSize, current * pageSize);

  if (params.sorter) {
    const sorter = JSON.parse(params.sorter);
    dataSource = dataSource.sort((prev, next) => {
      let sortNumber = 0;
      Object.keys(sorter).forEach((id) => {
        if (sorter[id] === 'descend') {
          if (prev[id] - next[id] > 0) {
            sortNumber += -1;
          } else {
            sortNumber += 1;
          }

          return;
        }

        if (prev[id] - next[id] > 0) {
          sortNumber += 1;
        } else {
          sortNumber += -1;
        }
      });
      return sortNumber;
    });
  }

  if (params.filter) {
    const filter = JSON.parse(params.filter);

    if (Object.keys(filter).length > 0) {
      dataSource = dataSource.filter((item) => {
        return Object.keys(filter).some((id) => {
          if (!filter[id]) {
            return true;
          }

          if (filter[id].includes(`${item[id]}`)) {
            return true;
          }

          return false;
        });
      });
    }
  }

  if (params.name) {
    dataSource = dataSource.filter((data) => data?.name?.includes(params.name || ''));
  }

  const result = {
    data: dataSource,
    total: tableListDataSource.length,
    success: true,
    pageSize,
    current: parseInt(`${params.current}`, 10) || 1,
  };
  return res.json(result);
}

function postRule(req, res, u, b) {
  let realUrl = u;

  if (!realUrl || Object.prototype.toString.call(realUrl) !== '[object String]') {
    realUrl = req.url;
  }

  const body = (b && b.body) || req.body;
  const { id } = body;
  const method = req.method.toLowerCase()
  console.log(body, method)

  switch (method) {
    /* eslint no-case-declarations:0 */
    case 'delete':
      const {id: rm_id} = req.query
      console.log(req.query)
      console.log('delete ' + rm_id)
      tableListDataSource = tableListDataSource.filter((item) => rm_id.indexOf(item.id) === -1);
      break;

    case 'post':
      (() => {
        const i = Math.ceil(Math.random() * 10000);
        const newRule = body
        newRule.id = tableListDataSource.length,
        tableListDataSource.unshift(newRule);
        return res.json(newRule);
      })();

      return;

    case 'patch':
      (() => {
        let newRule = {};
        tableListDataSource = tableListDataSource.map((item) => {
          if (item.id === id) {
            newRule = Object.assign(item, body)
            return newRule;
          }

          return item;
        });
        return res.json(newRule);
      })();

      return;

    default:
      break;
  }

  const result = {
    list: tableListDataSource,
    pagination: {
      total: tableListDataSource.length,
    },
  };
  res.json(result);
}

export default {
  'GET /api/user': getRule,
  'POST /api/user': postRule,
  'PATCH /api/user': postRule,
  'DELETE /api/user': postRule,
};
